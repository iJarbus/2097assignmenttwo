// Fill out your copyright notice in the Description page of Project Settings.

#include "CCharecter.h"
#include "AssignmentOne.h"
#include <stdexcept>
#include "UnrealNetwork.h"
#include "Collectable.h"
#include "Components/InputComponent.h"
#include "Engine/Engine.h"


// Sets default values
ACCharecter::ACCharecter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	// Create a first person camera component.
	FPSCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	// Attach the camera component to our capsule component.
	FPSCameraComponent->SetupAttachment(RootComponent);
	// Position the camera slightly above the eyes.
	FPSCameraComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 50.0f + BaseEyeHeight));
	// Allow the pawn to control camera rotation.
	FPSCameraComponent->bUsePawnControlRotation = true;

	// Create a first person mesh component for the owning player.
	FPSMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FirstPersonMesh"));
	// Only the owning player sees this mesh.
	FPSMesh->SetOnlyOwnerSee(true);
	// Attach the FPS mesh to the FPS camera.
	FPSMesh->SetupAttachment(FPSCameraComponent);
	// Disable some environmental shadowing to preserve the illusion of having a single mesh.
	FPSMesh->bCastDynamicShadow = false;
	FPSMesh->CastShadow = false;

	// The owning player doesn't see the regular (third-person) body mesh.
	GetMesh()->SetOwnerNoSee(true);
}


// Called when the game starts or when spawned
void ACCharecter::BeginPlay()
{
	Super::BeginPlay();
	// Put the users HUD on the screen.
	ChangeMenuWidget(theHUD);
}

// Called every frame
void ACCharecter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACCharecter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	// Movement bindings
	PlayerInputComponent->BindAxis("MoveForward", this, &ACCharecter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACCharecter::MoveRight);

	// Mouse control bindings
	PlayerInputComponent->BindAxis("Turn", this, &ACCharecter::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &ACCharecter::AddControllerPitchInput);

	// Set up "action" bindings.
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACCharecter::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACCharecter::StopJump);

	// Setup interact key
	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ACCharecter::Interact);

}

/*
This is the function that gets called whenever the an Unreal component tries to damage
an actor, it's being used here to make the pain causing volume work for the tick
damage being applied to the player.
*/
float ACCharecter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) {
	decrimentHealth(DamageAmount);
	return DamageAmount;
}

void ACCharecter::MoveForward(float Value) {
	// Find out which way is "forward" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
	AddMovementInput(Direction, Value);
}

void ACCharecter::MoveRight(float Value) {
	// Find out which way is "right" and record that the player wants to move that way.
	FVector Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::Y);
	AddMovementInput(Direction, Value);
}

void ACCharecter::StartJump() {
	bPressedJump = true;
}

void ACCharecter::StopJump() {
	bPressedJump = false;
}

void ACCharecter::Interact() {
	CallMyTrace();
}

/*
This function allows the health of the actor to be set directly. Only the server can do 
this, the health is then replicated out to the clients.

@param amount the value the health will be set to, this can't be negitive or greater
than the maximum health.

*/
void ACCharecter::setHealth(float amount) {
	if (Role == ROLE_Authority) {
		if (amount < 0) {
			throw std::invalid_argument("received negative value");
		}
		else if (amount > maxHealth) {
			throw std::invalid_argument("received value greater than max health");
		}
		else {
			currentHealth = amount;
		}
	}
}

/*
This function will increment the health of a player by a given amount, only positive
values may be given

@param amount the amount to increment health by.
*/
void ACCharecter::incrimentHealth(float amount) {
	if (amount < 0) {
		throw std::invalid_argument("negative value for increment function");
	}
	else if (currentHealth + amount > maxHealth) {
		ACCharecter::setHealth(maxHealth);
	}
	else {
		ACCharecter::setHealth(currentHealth + amount);
	}
}

/*
This function will detriment the health of a player by a given amount, only positive
values may be given

@param amount the amount to detriment health by.
*/
void ACCharecter::decrimentHealth(float amount) {
	if (amount < 0) {
		throw std::invalid_argument("negative value for decrement function");
	}
	else if (currentHealth + amount < 0) {
		ACCharecter::setHealth(0);
	}
	else {
		ACCharecter::setHealth(currentHealth - amount);
	}
}

/*
This function will return a string containing all elements in the players inventory
separated by newline characters

@return the formated string.
*/
FString ACCharecter::getFormatedInventoryList() {
	FString returnText = "";
	for (CollectableType itemType : Inventory) {
		if (itemType == CollectableType::Key) {
			returnText.Append("Key");
		}
		else if (itemType == CollectableType::Fuse) {
			returnText.Append("Fuse");
		}
		else {
			returnText.Append("Generic pickup");
		}
		returnText.Append("\n");
	}
	return returnText;
}

/*
Will return if a player has an item of a specific type.

@param itemType the item type to check for
@return a boolean indicating if the player has an item of that type
*/
bool ACCharecter::hasItem(TEnumAsByte<CollectableType> itemType) {
	for (CollectableType inventoryItemType : Inventory) {
		if (inventoryItemType == itemType) {
			return true;
		}
	}
	return false;
}

/*
Will return the actor hit by a trace from the player character without processing it.

@return the raw trace hit result.
*/
FHitResult ACCharecter::getTraceHit() {
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FPSCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FPSCameraComponent->GetForwardVector();

	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 256;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false);
	return HitData;
}

/*
Will remove the widget currently on the screen and replace it with a new one

@param NewWidgetClass the new widget to put on the screen
*/
void ACCharecter::ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass)
{
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromViewport();
		CurrentWidget = nullptr;
	}
	if (NewWidgetClass != nullptr)
	{
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), NewWidgetClass);
		if (CurrentWidget != nullptr)
		{
			CurrentWidget->AddToViewport();
		}
	}
}

/*
An Unreal engine function to replicate properties.
*/
void ACCharecter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACCharecter, currentHealth);
	DOREPLIFETIME(ACCharecter, Inventory);

}

/*
returns a string with the players authority, that is if they are a client or a server.

@return the string with the role
*/
FString ACCharecter::myRole() {
	if (Role == ROLE_Authority) {
		return TEXT("Server");
	}
	else {
		return TEXT("Client");
	}
}

/*
If validation passes this function will add the collectable type to the inventory
and then destroy the actor that is being collected.

@param collectable the collectable object being added to the inventory
*/
void ACCharecter::recieveItem_Implementation(ACollectable* collectable) {
	Inventory.Add(collectable->getCollectableType());
	collectable->Destroy();
}

/*
Validates if the collectable is valid to receive.

@param collectable the item being validated.
@return a boolean indicating if the function has been validated.
*/
bool ACCharecter::recieveItem_Validate(ACollectable* collectable) {
	if (!collectable) {
		return false;
	}
	return true;
}

//***************************************************************************************************
//** Trace functions - used to detect items we are looking at in the world
//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//** Trace() - called by our CallMyTrace() function which sets up our parameters and passes them through
//***************************************************************************************************

bool ACCharecter::Trace(
	UWorld* World,
	TArray<AActor*>& ActorsToIgnore,
	const FVector& Start,
	const FVector& End,
	FHitResult& HitOut,
	ECollisionChannel CollisionChannel = ECC_Pawn,
	bool ReturnPhysMat = false
) {

	// The World parameter refers to our game world (map/level) 
	// If there is no World, abort
	if (!World)
	{
		return false;
	}

	// Set up our TraceParams object
	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

	// Should we simple or complex collision?
	TraceParams.bTraceComplex = true;

	// We don't need Physics materials 
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;

	// Add our ActorsToIgnore
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	// When we're debugging it is really useful to see where our trace is in the world
	// We can use World->DebugDrawTraceTag to tell Unreal to draw debug lines for our trace
	// (remove these lines to remove the debug - or better create a debug switch!)
	const FName TraceTag("MyTraceTag");
	World->DebugDrawTraceTag = TraceTag;
	TraceParams.TraceTag = TraceTag;


	// Force clear the HitData which contains our results
	HitOut = FHitResult(ForceInit);

	// Perform our trace
	World->LineTraceSingleByChannel
	(
		HitOut,		//result
		Start,	//start
		End, //end
		CollisionChannel, //collision channel
		TraceParams
	);

	// If we hit an actor, return true
	return (HitOut.GetActor() != NULL);
}

//***************************************************************************************************
//** CallMyTrace() - sets up our parameters and then calls our Trace() function
//***************************************************************************************************

void ACCharecter::CallMyTrace()
{
	// Get the location of the camera (where we are looking from) and the direction we are looking in
	const FVector Start = FPSCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FPSCameraComponent->GetForwardVector();

	// How for in front of our character do we want our trace to extend?
	// ForwardVector is a unit vector, so we multiply by the desired distance
	const FVector End = Start + ForwardVector * 256;

	// Force clear the HitData which contains our results
	FHitResult HitData(ForceInit);

	// What Actors do we want our trace to Ignore?
	TArray<AActor*> ActorsToIgnore;

	//Ignore the player character - so you don't hit yourself!
	ActorsToIgnore.Add(this);

	// Call our Trace() function with the paramaters we have set up
	// If it Hits anything
	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		// Process our HitData
		if (HitData.GetActor())
		{

			//UE_LOG(LogClass, Warning, TEXT("This a testing statement. %s"), *HitData.GetActor()->GetName());
			ProcessTraceHit(HitData);

		}
		else
		{
			//UE_LOG(LogClass, Warning, TEXT("Trace error. %s"), *HitData.GetActor()->GetName());
		}
	}
	else
	{
		// We did not hit an Actor
		ClearPickupInfo();

		//UE_LOG(LogClass, Warning, TEXT("No actor hit. %s"), *HitData.GetActor()->GetName());

	}

}

//***************************************************************************************************
//** ProcessTraceHit() - process our Trace Hit result
//***************************************************************************************************

void ACCharecter::ProcessTraceHit(FHitResult& HitOut)
{

	// Cast the actor to APickup
	ACollectable* const CollectableTraceHit = Cast<ACollectable>(HitOut.GetActor());

	if (CollectableTraceHit)
	{
		// Keep a pointer to the Pickup
		CurrentPickup = CollectableTraceHit;

		// Set a local variable of the PickupName for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupName: %s"), *TestPickup->GetPickupName());
		PickupName = CollectableTraceHit->getPickupName();

		// Set a local variable of the PickupDisplayText for the HUD
		//UE_LOG(LogClass, Warning, TEXT("PickupDisplayText: %s"), *TestPickup->GetPickupDisplayText());
		PickupDisplayText = CollectableTraceHit->getPickupDisplayText();
		PickupFound = true;
		if (CollectableTraceHit->getIsPickupAble()) {
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("comsume called"));
			CollectableTraceHit->consume(this, CollectableTraceHit);
		}
	}
	else
	{
		UE_LOG(LogClass, Warning, TEXT("TestPickup is NOT a Pickup!"));
		ClearPickupInfo();
	}
}

/*
reset all the pickup info
*/
void ACCharecter::ClearPickupInfo() {
	PickupFound = false;
	PickupDisplayText = "";
	PickupName = "";
}