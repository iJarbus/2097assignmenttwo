// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CDoor.generated.h"

UCLASS()
class ASSIGNMENTONE_API ACDoor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACDoor();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Replicated)
		bool isLocked = true;
	
	UFUNCTION(BlueprintCallable, Category = "door")
		FString GetMessage();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString lockedMessage = "Door locked";
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString unLockedMessage = "Press 'E' to open";

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	
	
};
