// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/UserWidget.h"
#include "CGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ASSIGNMENTONE_API ACGameMode : public AGameModeBase
{
	GENERATED_BODY()
	virtual void StartPlay() override;
	
public:

protected:
	/** Called when the game starts. */
	virtual void BeginPlay() override;
};
