// Fill out your copyright notice in the Description page of Project Settings.

#include "CDoor.h"
#include "UnrealNetwork.h"


// Sets default values
ACDoor::ACDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

FString ACDoor::GetMessage() {
	if (isLocked) {
		return lockedMessage;
	}
	else {
		return unLockedMessage;
	}
}

// Called when the game starts or when spawned
void ACDoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACDoor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACDoor, isLocked);
}

