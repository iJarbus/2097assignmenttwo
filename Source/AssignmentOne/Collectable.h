// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/TextRenderComponent.h"
#include "Components/SphereComponent.h"
#include "Collectable.generated.h"

UENUM()
enum CollectableType {
	GenericPickup     UMETA(DisplayName = "Generic pickup"),
	Key     UMETA(DisplayName = "key"),
	Fuse     UMETA(DisplayName = "Fuse"),
};

UCLASS()
class ASSIGNMENTONE_API ACollectable : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACollectable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString name = "Generic pickup";
	
	//This stores if the item is in a pickup able state, for now this is if the player
	//is standing close enough.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Replicated)
		bool isPickupAble = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString lookAtText = "Generic pickup";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = pickupType)
		TEnumAsByte<CollectableType> pickupType;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual FString getPickupName() { return name; }
	virtual FString getPickupDisplayText();
	virtual TEnumAsByte<CollectableType> getCollectableType() { return pickupType; }


	virtual bool getIsPickupAble() { return isPickupAble; }

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UFUNCTION(BlueprintCallable, Category = "collectable")
		void setIsPickupAble(bool state);

	// declare overlap begin function
	UFUNCTION()
		void onOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// declare overlap end function
	UFUNCTION()
		void onOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//The sphere that allows the collectible tell if the player is standing close to it.
	UPROPERTY(VisibleAnywhere)
		class USphereComponent* collisionSphere;

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable, WithValidation, Category = "collectable")
		void consume(ACCharecter* player, ACollectable* theItem);
};