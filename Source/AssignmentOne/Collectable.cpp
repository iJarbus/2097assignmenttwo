// Fill out your copyright notice in the Description page of Project Settings.

#include "Collectable.h"
#include "UnrealNetwork.h"
// include draw debug helpers header file
#include "DrawDebugHelpers.h"
#include "Engine/Engine.h"
#include "CCharecter.h"

// Sets default values
ACollectable::ACollectable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	//This is the collision sphere that tells detects if the player is close enough to
	//the item to pick it up.
	collisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));

	collisionSphere->InitSphereRadius(250.f);
	collisionSphere->SetCollisionProfileName("Trigger");


	RootComponent = collisionSphere;
}

// Called when the game starts or when spawned
void ACollectable::BeginPlay() {
	Super::BeginPlay();
	
	// These must be added in the begin play function and not the constructor, not sure
	//why Unreal is odd.
	collisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ACollectable::onOverlapBegin);
	collisionSphere->OnComponentEndOverlap.AddDynamic(this, &ACollectable::onOverlapEnd);
}

// Called every frame
void ACollectable::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	//This creates an
}

//Returns the text that should be displayed when the player hovers over it.
FString ACollectable::getPickupDisplayText() {
	//For now we're just displaying the name of the pickup.
	return getPickupName();
}

//Makes Unreal happy for replicated variables
void ACollectable::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ACollectable, isPickupAble);
}

//Only the server can set if the collectible is currently in a pickup ale state
void ACollectable::setIsPickupAble(bool state) {
	if (Role == ROLE_Authority) {
		isPickupAble = state;
	}
}

void ACollectable::onOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (Role == ROLE_Authority) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Begin"));
		isPickupAble = true;
	}
}

void ACollectable::onOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (Role == ROLE_Authority) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("End"));
		isPickupAble = false;
	}
}

/*
the implementation of the consume function if the validation passes, we're using 
a reference to the item as opposed to a reference to self since Unreal would crash, not
exactly sure why, Unreal is odd sometimes.
*/
void ACollectable::consume_Implementation(ACCharecter* player, ACollectable* theItem) {
	player->recieveItem(theItem);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("consume exacuted"));
	//theItem->Destroy();
}

//This function is here to make Unreal happy, always returns true.
bool ACollectable::consume_Validate(ACCharecter* player, ACollectable* theItem) {
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("consume validated"));
	return true;
}

//void ACollectable::consume(ACCharecter* player) {
//	player->recieveItem(this);
//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("collectable is being consumed"));
//	this->Destroy();
//}
