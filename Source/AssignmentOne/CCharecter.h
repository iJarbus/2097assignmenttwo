// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "Blueprint/UserWidget.h"
#include "Collectable.h"
#include "Array.h"
#include "CCharecter.generated.h"

UCLASS()
class ASSIGNMENTONE_API ACCharecter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACCharecter();
	// Health statistics for the player
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float maxHealth = 100;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
		float currentHealth = 80;
	//items the player currently has
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Replicated)
		TArray<TEnumAsByte<CollectableType>> Inventory;
	
//Statistic getters and setters for the player
public:
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Player")
		float getHealth() { return currentHealth; }
	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Player")
		float getMaxHealth() { return maxHealth; }

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Player")
		FString myRole();

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "Player")
		void recieveItem(ACollectable* collectable);

	UFUNCTION(BlueprintCallable, Category = "Player")
		void incrimentHealth(float amount);
	UFUNCTION(BlueprintCallable, Category = "Player")
		void decrimentHealth(float amount);

	UFUNCTION(BlueprintCallable, Category = "Player")
		FString getFormatedInventoryList();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Remove the current menu widget and create a new one from the specified class, if provided. */
	UFUNCTION(BlueprintCallable, Category = "UMG Game")
		void ChangeMenuWidget(TSubclassOf<UUserWidget> NewWidgetClass);

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	// Trace Functionality
	bool Trace(
		UWorld* World,
		TArray<AActor*>& ActorsToIgnore,
		const FVector& Start,
		const FVector& End,
		FHitResult& HitOut,
		ECollisionChannel CollisionChannel,
		bool ReturnPhysMat
	);
	void CallMyTrace();
	void ProcessTraceHit(FHitResult& HitOut);
	void ClearPickupInfo();

	ACollectable* CurrentPickup;
	bool PickupFound;
	FString PickupDisplayText;
	FString PickupName;

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Player")
		bool hasItem(TEnumAsByte<CollectableType> itemType);

	UFUNCTION(BlueprintPure, BlueprintCallable, Category = "Player")
		FHitResult getTraceHit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The widget that will be used as the HUD for the player
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UMG Game")
		TSubclassOf<UUserWidget> theHUD;

	/** The widget instance that we are using as our menu. */
	UPROPERTY()
		UUserWidget* CurrentWidget;
	
	// Handles network replication
	void setHealth(float amount);

public:	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	//Movement functions//
	//Forward and backward
	UFUNCTION()
		void MoveForward(float Value);

	//Left right
	UFUNCTION()
		void MoveRight(float Value);

	// Sets jump flag when key is pressed.
	UFUNCTION()
		void StartJump();

	// Clears jump flag when key is released.
	UFUNCTION()
		void StopJump();

	// Player has pressed interact key
	UFUNCTION()
		void Interact();

	// FPS camera.
	UPROPERTY(VisibleAnywhere)
		UCameraComponent* FPSCameraComponent;

	// First-person mesh (arms), visible only to the owning player.
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* FPSMesh;
};